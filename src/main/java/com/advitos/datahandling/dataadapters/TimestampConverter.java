package com.advitos.datahandling.dataadapters;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * Converts a LabView time object into {@link Timestamp}.
 *
 * @author kaja
 * @version 1.0
 */
@NoArgsConstructor
@Component
public class TimestampConverter {

    private static final long LAB_VIEW_UTC = -2082844800;

    public Timestamp convertToSqlTimestamp(long seconds, long nano) {

        Instant instant = Instant.ofEpochSecond(seconds, nano);
        int nanos = instant.getNano();
        long unixUTC = LAB_VIEW_UTC + seconds;

        Instant i = Instant.ofEpochSecond(unixUTC);

        Timestamp utc = Timestamp.from(i);
        utc.setNanos(nanos);
        return utc;
    }

    public long convertToLong(long seconds, long nano) {
        Instant epochSecond = this.convertToSqlTimestamp(seconds, nano).toInstant();
        return epochSecond.getEpochSecond();

    }

}

