package com.advitos.datahandling.dataadapters;

import com.advitos.datahandling.BinaryLabviewData;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Map;

@Log4j2
public class LabViewDataJsonDeserializer implements Deserializer<BinaryLabviewData> {

    private final Class <BinaryLabviewData> type;

    public LabViewDataJsonDeserializer(Class<BinaryLabviewData> type) {
        this.type = type;
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Deserializer.super.configure(configs, isKey);
    }

    @Override
    public BinaryLabviewData deserialize(String topic, byte[] data) {
        BinaryLabviewData object = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            object = mapper.readValue(data, type);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return object;
    }

    @Override
    public BinaryLabviewData deserialize(String topic, Headers headers, byte[] data) {
        return Deserializer.super.deserialize(topic, headers, data);
    }

    @Override
    public void close() {
        Deserializer.super.close();
    }
}
