package com.advitos.datahandling.dataadapters;

import com.advitos.datahandling.BinaryLabviewData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

@Log4j2
public class LabViewDataJsonSerializer implements Serializer<BinaryLabviewData> {


    @Override
    public void configure(Map configs, boolean isKey) {
        Serializer.super.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, BinaryLabviewData data) {
        byte[] value = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            value = mapper.writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return value;
    }

    @Override
    public void close() {
        Serializer.super.close();
    }
}
