package com.advitos.datahandling;
import io.netty.buffer.ByteBuf;

public interface BinaryLabviewData {

	void parse(ByteBuf buffer);
	
}
