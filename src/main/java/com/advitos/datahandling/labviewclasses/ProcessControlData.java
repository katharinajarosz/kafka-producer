/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.ManagedBean;

@Getter @Setter
@ManagedBean
public class ProcessControlData implements BinaryLabviewData {

	private byte systemState;
	private byte idleState;
	private byte startupState;
	private byte serviceState;
	private byte preparationState;
	private byte treatmentState;
	private byte postprocessState;
	private byte desinfectionState;
	private byte activeSubsequence;
	private byte subsequenceState;

	@Override
	public void parse(ByteBuf buffer) {
		systemState=buffer.readByte();
		idleState=buffer.readByte();
		startupState=buffer.readByte();
		serviceState=buffer.readByte();
		preparationState=buffer.readByte();
		treatmentState=buffer.readByte();
		postprocessState=buffer.readByte();
		desinfectionState=buffer.readByte();
		activeSubsequence=buffer.readByte();
		subsequenceState=buffer.readByte();
	}

	@Override
	public String toString() {
		return "\n ProcessControlData {" +
				" systemState=" + systemState +
				", idleState=" + idleState +
				", startupState=" + startupState +
				", serviceState=" + serviceState +
				", preparationState=" + preparationState +
				", treatmentState=" + treatmentState +
				", postprocessState=" + postprocessState +
				", desinfectionState=" + desinfectionState +
				", activeSubsequence=" + activeSubsequence +
				", subsequenceState=" + subsequenceState +
				'}';
	}
}
