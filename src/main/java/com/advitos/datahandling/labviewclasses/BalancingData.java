/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.ManagedBean;

@Getter @Setter
@ManagedBean
public class BalancingData implements BinaryLabviewData {
	
	private float ufHost;
	private float ufControl;
	private float ufWeight;
	private float acidFlowConrol;
	private float acidWeight;
	private float acidDensity;
	private float baseFlowControl;
	private float baseWeight;
	private float baseDensity;
	private float scale1row;
	private float scale2row;
	private float scaleDeviation;
	private float scaleSumCurrent;
	private float balanceOffset;

	@Override
	public void parse(ByteBuf buffer) {
		ufHost=buffer.readFloat();
		ufControl=buffer.readFloat();
		ufWeight=buffer.readFloat();
		acidFlowConrol=buffer.readFloat();
		acidWeight=buffer.readFloat();
		acidDensity=buffer.readFloat();
		baseFlowControl=buffer.readFloat();
		baseWeight=buffer.readFloat();
		baseDensity=buffer.readFloat();
		scale1row=buffer.readFloat();
		scale2row=buffer.readFloat();
		scaleDeviation=buffer.readFloat();
		scaleSumCurrent=buffer.readFloat();
		balanceOffset=buffer.readFloat();

	}

	@Override
	public String toString() {
		return "\n BalancingData {" +
				"ufHost=" + ufHost +
				", ufControl=" + ufControl +
				", ufWeight=" + ufWeight +
				", acidFlowConrol=" + acidFlowConrol +
				", acidWeight=" + acidWeight +
				", acidDensity=" + acidDensity +
				", baseFlowControl=" + baseFlowControl +
				", baseWeight=" + baseWeight +
				", baseDensity=" + baseDensity +
				", scale1row=" + scale1row +
				", scale2row=" + scale2row +
				", scaleDeviation=" + scaleDeviation +
				", scaleSumCurrent=" + scaleSumCurrent +
				", balanceOffset=" + balanceOffset +
				'}';
	}
}
