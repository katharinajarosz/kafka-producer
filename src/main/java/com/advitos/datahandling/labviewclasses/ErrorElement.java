/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ErrorElement implements BinaryLabviewData {

	private int errorId;
	private int errorLevel;

	@Override
	public void parse(ByteBuf buffer) {
		errorId=buffer.readInt();
		errorLevel=buffer.readInt();
	}

	@Override
	public String toString() {
		return "\n ErrorElement {" +
				" errorId=" + errorId +
				", errorLevel=" + errorLevel +
				'}';
	}
}
