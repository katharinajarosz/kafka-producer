/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AnalogIn implements BinaryLabviewData {

	private float f;
	private float cd;

	@Override
	public void parse(ByteBuf buffer) {
		f=buffer.readFloat();
		cd=buffer.readFloat();
	}

}
