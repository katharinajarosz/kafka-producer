/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.ManagedBean;

@Getter @Setter
@ManagedBean
public class SystemDiagnosis implements BinaryLabviewData {

	private float processorLoad;
	private float memoryUsage;
	private float systemLoopProcessingTime;
	private float loopLateCounter;
	private float hostTotalProcessorLoad;
	private float hostLK2001ProcessorLoad;
	private float hostLK2001PrivateBytes;
	private float hostReconnectCounter;
	private float hostConnectionStageChangeCounter;
	private float durationLastSavingProcess;
	private float open;
	private float setPointerToEnd;
	private float sdwrite;
	private float sdflush;
	private float sdclose;
	private float elementInQ;

	@Override
	public void parse(ByteBuf buffer) {
		processorLoad=buffer.readFloat();
		memoryUsage=buffer.readFloat();
		systemLoopProcessingTime=buffer.readFloat();
		loopLateCounter=buffer.readFloat();
		hostTotalProcessorLoad=buffer.readFloat();
		hostLK2001ProcessorLoad=buffer.readFloat();
		hostLK2001PrivateBytes=buffer.readFloat();
		hostReconnectCounter=buffer.readFloat();
		hostConnectionStageChangeCounter=buffer.readFloat();
		durationLastSavingProcess=buffer.readFloat();
		open=buffer.readFloat();
		setPointerToEnd=buffer.readFloat();
		sdwrite=buffer.readFloat();
		sdflush=buffer.readFloat();
		sdclose=buffer.readFloat();
		elementInQ=buffer.readFloat();
	}

	@Override
	public String toString() {
		return "\n SystemDiagnosis {" +
				" processorLoad=" + processorLoad +
				", memoryUsage=" + memoryUsage +
				", systemLoopProcessingTime=" + systemLoopProcessingTime +
				", loopLateCounter=" + loopLateCounter +
				", hostTotalProcessorLoad=" + hostTotalProcessorLoad +
				", hostLK2001ProcessorLoad=" + hostLK2001ProcessorLoad +
				", hostLK2001PrivateBytes=" + hostLK2001PrivateBytes +
				", hostReconnectCounter=" + hostReconnectCounter +
				", hostConnectionStageChangeCounter=" + hostConnectionStageChangeCounter +
				", durationLastSavingProcess=" + durationLastSavingProcess +
				", open=" + open +
				", setPointerToEnd=" + setPointerToEnd +
				", sdwrite=" + sdwrite +
				", sdflush=" + sdflush +
				", sdclose=" + sdclose +
				", elementInQ=" + elementInQ +
				'}';
	}
}
