package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter @Setter
public class NumericOutput implements BinaryLabviewData {

    private String device;
    private Timestamp pi_timestamp;
    private int type;
    private float dem;
    private float cd;

    @Override
    public void parse(ByteBuf buffer) {
        type=buffer.readInt();
        dem=buffer.readFloat();
        cd=buffer.readFloat();
    }

}
