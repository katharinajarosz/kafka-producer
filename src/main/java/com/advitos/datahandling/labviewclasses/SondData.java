/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SondData implements BinaryLabviewData {

	private short upH;
	private short uTemp;
	private float temperature;
	private float ph;

	@Override
	public void parse(ByteBuf buffer) {
		upH=buffer.readShort();
		uTemp=buffer.readShort();
		temperature=buffer.readFloat();
		ph=buffer.readFloat();
	}

	@Override
	public String toString() {
		return "\n SondData {" +
				" upH=" + upH +
				", uTemp=" + uTemp +
				", temperature=" + temperature +
				", ph=" + ph +
				'}';
	}
}
