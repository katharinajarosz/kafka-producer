package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter @Setter
public class ClosedLoop implements BinaryLabviewData {

	@JsonProperty
	private String device;
	@JsonProperty
	private Timestamp pi_timestamp;
	@JsonProperty
	private int type;
	@JsonProperty
	private float hmi;
	@JsonProperty
	private float proc;
	@JsonProperty
	private float dem;
	@JsonProperty
	private float y;
	@JsonProperty
	private float inp;
	@JsonProperty
	private float e;
	
	
	@Override
	public void parse(ByteBuf buffer) {
		type=buffer.readInt();
		hmi=buffer.readFloat();
		proc=buffer.readFloat();
		dem=buffer.readFloat();
		y=buffer.readFloat();
		inp=buffer.readFloat();
		e=buffer.readFloat();
		
	}

}
