/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@ManagedBean
public class ProtectiveSystemData implements BinaryLabviewData {
	
	private int unknownMessageCount;
	private int crcErrorCount;
	private int messageCounterErrorCount;
	private byte sendMsgCounter;
	private byte protectState;
	private byte protectSubstate;
	private byte controlStatusByte;
	private byte protectStatusByte;
	private List<SondData> sondData = new ArrayList<>();
	private List<PumpData> pumpData = new ArrayList<>();
	private List<AnalogIn> analogIn = new ArrayList<>();
	private byte lastReceivedMessageID;
	private byte lastSendMessageID;
	private byte lastSendMainState;
	private byte lastSendSubState;
	private int rs232CommunicationLoopExecutionTime;
	private BalancingData balancingData = new BalancingData();

	@Override
	public void parse(ByteBuf buffer) {
		unknownMessageCount=buffer.readInt();
		crcErrorCount=buffer.readInt();
		messageCounterErrorCount=buffer.readInt();
		sendMsgCounter=buffer.readByte();
		protectState=buffer.readByte();
		protectSubstate=buffer.readByte();
		controlStatusByte=buffer.readByte();
		protectStatusByte=buffer.readByte();
		for (int i = 0; i < 4; i++) {
			SondData sd = new SondData();
			sd.parse(buffer);
			sondData.add(sd);
		}
		for (int i = 0; i < 2; i++) {
			PumpData pd = new PumpData();
			pd.parse(buffer);
			pumpData.add(pd);
		}
		for (int i = 0; i < 10; i++) {
			AnalogIn ai = new AnalogIn();
			ai.parse(buffer);
			analogIn.add(ai);
		}
		lastReceivedMessageID=buffer.readByte();
		lastSendMessageID=buffer.readByte();
		lastSendMainState=buffer.readByte();
		lastSendSubState=buffer.readByte();
		rs232CommunicationLoopExecutionTime=buffer.readInt();
		balancingData.parse(buffer);
	}

	@Override
	public String toString() {
		return "\n ProtectiveSystemData{" +
				" unknownMessageCount=" + unknownMessageCount +
				", crcErrorCount=" + crcErrorCount +
				", messageCounterErrorCount=" + messageCounterErrorCount +
				", sendMsgCounter=" + sendMsgCounter +
				", protectState=" + protectState +
				", protectSubstate=" + protectSubstate +
				", controlStatusByte=" + controlStatusByte +
				", protectStatusByte=" + protectStatusByte +
				", sondData=" + sondData +
				", pumpData=" + pumpData +
				", analogIn=" + analogIn +
				", lastReceivedMessageID=" + lastReceivedMessageID +
				", lastSendMessageID=" + lastSendMessageID +
				", lastSendMainState=" + lastSendMainState +
				", lastSendSubState=" + lastSendSubState +
				", rs232CommunicationLoopExecutionTime=" + rs232CommunicationLoopExecutionTime +
				", balancingData=" + balancingData +
				'}';
	}
}
