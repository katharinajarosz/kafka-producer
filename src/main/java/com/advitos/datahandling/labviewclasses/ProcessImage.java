package com.advitos.datahandling.labviewclasses;


import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter @Setter
@ManagedBean
public class ProcessImage implements BinaryLabviewData {

    private List<NumericInput> numericInputs = new ArrayList<>();
    private List<DigitalInput> digitalInputs = new ArrayList<>();
    private List<ClosedLoop> closedLoops = new ArrayList<>();
    private List<NumericOutput> numericOutputs = new ArrayList<>();
    private List<DigitalOutput> digitalOutputs = new ArrayList<>();
    private IOData ioData = new IOData();
    private ProcessControlData processControlData = new ProcessControlData();
    private ProtectiveSystemData protectiveSystemData = new ProtectiveSystemData();
    private SystemDiagnosis systemDiagnosis = new SystemDiagnosis();
    private ErrorData errorData = new ErrorData();

    private String device;
    private long utcTimestamp;
    private Timestamp sqlTimestamp;

    @Override
    public void parse(ByteBuf buffer) {
        int numericInput = buffer.readInt();
        for (int i = 0; i < numericInput; i++) {
            NumericInput ni = new NumericInput();
            ni.parse(buffer);
            numericInputs.add(ni);
        }
        int digitalInput = buffer.readInt();
        for (int i = 0; i < digitalInput; i++) {
            DigitalInput di = new DigitalInput();
            di.parse(buffer);
            digitalInputs.add(di);
        }
        int closedLoop = buffer.readInt();
        for (int i = 0; i < closedLoop; i++) {
            ClosedLoop cl = new ClosedLoop();
            cl.parse(buffer);
            closedLoops.add(cl);
        }
        int numericOutput = buffer.readInt();
        for (int i = 0; i < numericOutput; i++) {
            NumericOutput no = new NumericOutput();
            no.parse(buffer);
            numericOutputs.add(no);
        }
        int digitalOutput = buffer.readInt();
        for (int i = 0; i < digitalOutput; i++) {
            DigitalOutput dou = new DigitalOutput();
            dou.parse(buffer);
            digitalOutputs.add(dou);
        }
        ioData.parse(buffer);
        processControlData.parse(buffer);
        protectiveSystemData.parse(buffer);
        systemDiagnosis.parse(buffer);
        errorData.parse(buffer);
    }

    @Override
    public String toString() {
        return "\n ProcessImage {" +
                " ioData=" + ioData +
                ", processControlData=" + processControlData +
                ", protectiveSystemData=" + protectiveSystemData +
                ", systemDiagnosis=" + systemDiagnosis +
                ", errorData=" + errorData +
                '}';
    }
}
