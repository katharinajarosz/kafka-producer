package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter @Setter
public class NumericInput implements BinaryLabviewData {

	@JsonProperty
	private String device;
	@JsonProperty
	private Timestamp pi_timestamp;
	@JsonProperty
	private int type;
	@JsonProperty
	private float r;
	@JsonProperty
	private float f;
	@JsonProperty
	private float cd;
	
	@Override
	public void parse(ByteBuf buffer) {
		type =buffer.readInt();
		r=buffer.readFloat();
		f=buffer.readFloat();
		cd=buffer.readFloat();
	}

}
