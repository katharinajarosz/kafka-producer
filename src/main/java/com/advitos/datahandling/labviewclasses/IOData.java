/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Bean;

import javax.annotation.ManagedBean;

@Getter @Setter
@ManagedBean
public class IOData implements BinaryLabviewData {

	private long timestamp1;
	private long timestamp2;
	private byte supplyMode;
	private byte wasteMode;
	private byte gpMode;
	private byte heaterMode;

	@Override
	public void parse(ByteBuf buffer) {
		timestamp1=buffer.readLong();
		timestamp2=buffer.readLong();
		supplyMode=buffer.readByte();
		wasteMode=buffer.readByte();
		gpMode=buffer.readByte();
		heaterMode=buffer.readByte();
	}

	@Override
	public String toString() {
		return "\n IOData {" +
				" timestamp1=" + timestamp1 +
				", timestamp2=" + timestamp2 +
				", supplyMode=" + supplyMode +
				", wasteMode=" + wasteMode +
				", gpMode=" + gpMode +
				", heaterMode=" + heaterMode +
				'}';
	}
}
