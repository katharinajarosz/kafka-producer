package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter @Setter
public class DigitalOutput implements BinaryLabviewData {

    private String device;
    private Timestamp pi_timestamp;
    private int type;
    private boolean hmi;
    private boolean dem;

    @Override
    public void parse(ByteBuf buffer) {
        type=buffer.readInt();
        byte cHmi=buffer.readByte();
        hmi=(cHmi==1);
        byte cDem=buffer.readByte();
        dem=(cDem==1);
    }

}
