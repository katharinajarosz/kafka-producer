/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PumpData implements BinaryLabviewData {

	private float host;
	private float control;
	private float meas;

	@Override
	public void parse(ByteBuf buffer) {
		host=buffer.readFloat();
		control=buffer.readFloat();
		meas=buffer.readFloat();
	}

	@Override
	public String toString() {
		return "\n PumpData {" +
				" host=" + host +
				", control=" + control +
				", meas=" + meas +
				'}';
	}
}
