package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter @Setter
public class DigitalInput implements BinaryLabviewData {

	private String device;
	private Timestamp pi_timestamp;
	private int type;
	private boolean cd;
	private boolean r;
	
	@Override
	public void parse(ByteBuf buffer) {
		type =buffer.readInt();
		byte cdB=buffer.readByte();
		cd=(cdB==1);
		byte realValB=buffer.readByte();
		r=(realValB==1);
	}

}
