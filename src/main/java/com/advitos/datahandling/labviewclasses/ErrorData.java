/**
 * 
 */
package com.advitos.datahandling.labviewclasses;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.ManagedBean;
import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@ManagedBean
public class ErrorData implements BinaryLabviewData {

	private List<ErrorElement> errorElements = new ArrayList<>();
	private float phReservoirRangeCheck;

	@Override
	public void parse(ByteBuf buffer) {
		int size = buffer.readInt();
		for (int i = 0; i < size; i++) {
			ErrorElement e = new ErrorElement();
			e.parse(buffer);
			errorElements.add(e);
		}
		phReservoirRangeCheck=buffer.readFloat();
	}

	@Override
	public String toString() {
		return "\n ErrorData {" +
				" errorElementsSize=" + errorElements.size() +
				" errorElements=" + errorElements +
				", phReservoirRangeCheck=" + phReservoirRangeCheck +
				'}';
	}
}
