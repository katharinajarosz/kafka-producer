package com.advitos.datahandling;

import com.advitos.datahandling.dataadapters.TimestampConverter;
import com.advitos.datahandling.labviewclasses.ProcessImage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Stream;

@Slf4j
@Component
public class FileParser {

    private static final Path hwBin = Paths.get("C:/", "HWBIN");
    private static final int HEADER_SIZE = 1024;
    private static final String FILE_PATTERN = "glob:*.hwbin";

    @Autowired
    private TimestampConverter converter;

    @Autowired
    public FileParser() {}

    public List<ProcessImage> scanFileSystem() {
        List<ProcessImage> pi = new ArrayList<>();
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
        Queue<Path> pathQueue = new ConcurrentLinkedQueue<>();
        try {
            Stream<Path> result = Files.walk(hwBin);
            result
                    .filter(Files::isRegularFile)
                    .filter(path -> matcher.matches(path.getFileName()))
                    .forEach(pathQueue::add);
            this.readProcessImageData(pi, pathQueue);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Array list size: " + pi.size());
        return pi;
    }

    private void readProcessImageData(List<ProcessImage> processImages, Queue<Path> pathQueue) throws FileNotFoundException, InterruptedException {
        while (!pathQueue.isEmpty()) {
            Path path = pathQueue.poll();

            try (BufferedInputStream fileInputStream = new BufferedInputStream(new FileInputStream(path.toFile()))) {

                String file = path.toFile().toString();
                log.info("Path: " + file);
                String[] fileSplit = file.split("_");
                String device = fileSplit[6];
                fileInputStream.readNBytes(HEADER_SIZE);
                byte[] intByteArr = new byte[4];

                while (fileInputStream.read(intByteArr) != -1) {

                    int fromByteArray = Unpooled.copiedBuffer(intByteArr).getInt(0);
                    // log.info("Buffer size: " + fromByteArray);
                    byte[] data = new byte[fromByteArray + 1];
                    ProcessImage pi = getProcessImage(fileInputStream, data, device);

                    processImages.add(pi);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @org.jetbrains.annotations.NotNull
    private ProcessImage getProcessImage(BufferedInputStream fileInputStream, byte[] data, String device) throws IOException {
        fileInputStream.read(data);
        ByteBuf nettyBuffer = Unpooled.copiedBuffer(data);
        ProcessImage pi = new ProcessImage();
        pi.parse(nettyBuffer);
        pi.setDevice(device);
        pi.setSqlTimestamp(converter.convertToSqlTimestamp(pi.getIoData().getTimestamp1(), pi.getIoData().getTimestamp2()));
        pi.setUtcTimestamp(converter.convertToLong(pi.getIoData().getTimestamp1(), pi.getIoData().getTimestamp2()));
        return pi;
    }

     /* public static void main(String[] args) {
        try {
            FileParser test = new FileParser();
            log.info("START");
            test.scanFileSystem();
            log.info("END");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

      */

}
