package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCPumpData implements BinaryLabviewData {

    double host;
    double control;
    double meas;
    double gain;


    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // host
        buffer.readDouble(); // control
        buffer.readDouble(); // meas
        buffer.readDouble(); // gain

    }
}
