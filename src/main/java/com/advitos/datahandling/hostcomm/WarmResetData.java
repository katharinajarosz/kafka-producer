package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class WarmResetData implements BinaryLabviewData {

    // EB #8
    int System_State;
    int Idle_State;
    int Startup_State;
    int Service_State;
    int Preparation_State;
    int Treatment_State;
    int Postprocess_States;
    int Disinfection_State;
    // Boolean #3
    boolean StartupTestPassed;
    boolean T1TestPassed;
    boolean SystemExecutionFinished;
    // DBL #3
    double CL_BALANCE_PP_WASTE_Y;
    double CL_BALANCE_PP_WASTE_E;
    double CL_BALANCE_PP_WASTE_DEM;
    // Timestamp
    long PatientTreatmentBeginn1;
    long PatientTreatmentBeginn2;
    // DBL #21
    double PatientTreatmentDuration;
    double C_UF_VOLUME_CD;
    double C_RO_VOLUME_CD;
    double C_WASTE_VOLUME_CD;
    double C_ACID_VOLUME_CD;
    double C_BASE_VOLUME_CD;
    double C_BLOOD_VOLUME_CD;
    double C_EFF_BLOOD_VOLUME_CD;
    double C_PRE_VOLUME_CD;
    double C_RO_VOLUME_FLOW_CD;
    double CL_HEATER_HW_HMI;
    double CL_HEATER_HW_Y;
    double CL_HEATER_BASE_Y;
    double CL_PH_ACID_Y;
    double CL_PH_BASE_Y;
    double CL_PH_RES_HMI;
    double CL_PH_RES_Y;
    double CL_SUPPLY_FLOW_HMI;
    double CL_FLOW_BLOOD_PUMP_HMI;
    double CL_FLOW_PRE_DILUTION_PUMP_HMI;
    double CL_FLOW_UF_PUMP_HMI;
    // Timestamp
    long ErrH_BloodPumpStoppedTime1;
    long ErrH_BloodPumpStoppedTime2;
    // DBL #4
    double FlowDialyzer;
    double PhAcid;
    double PhBase;
    double HeaterBase;
    // Boolean #4
    boolean TreatmentExpertModeActive;
    boolean TreatmentPhAutomaticState;
    boolean TreatmentTemperatureAutomaticState;
    boolean AutomaticVolumeResetExecuted;
    // DBL #12
    double CL_PH_BASE_HMI;
    double CL_PH_ACID_HMI;
    double CL_FLOW_DIA_HMI;
    double CL_HEATER_BASE_HMI;
    double CL_PERCENTAGE_HEATER_BASE_HMI;
    double CL_PERCENTAGE_HEATER_HW_HMI;
    double CL_FLOW_HW_ACID_HMI;
    double CL_FLOW_HW_BASE_HMI;
    double pH_AutomaticStartTimer;
    double CL_PH_RES_Y_MEAN;
    double CL_PH_RES_Y_RANGE;
    double NextCrossswitch;
    // Boolean #3
    boolean FirstCrossswitchExecuted;
    boolean TreatmentPredilutionActive;
    boolean InitialStartAutomaticComplete;
    // DBL #2
    double HW_ACID_FLOW_MEAN;
    double HW_BASE_FLOW_MEAN;
    // Boolean
    boolean TreatmentFirstPatientConnectionComplete;
    // U8
    byte RemainingConcentrateErrorResets;
    // EB
    int Treatment_Mode;
    // Timestamp
    long CirculationBegin1;
    long CirculationBegin2;
    // DBL
    double CirculationDuration;
    // Boolean #2
    boolean TreatmentStartHepaWashForTheFirstTime;
    boolean Init2_Selected;
    // DBL #5
    double C_SUPPLY_VOLUME_CD;
    double C_UF_PATIENT_VOLUME_CD;
    double C_UF_ADDITIONS_VOLUME_CD;
    double CL_FLOW_UF_PATIENT_HMI;
    double CL_FLOW_UF_ADDITIONS_HMI;


    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // System_State
        buffer.readInt(); // Idle_State
        buffer.readInt(); // Startup_State
        buffer.readInt(); // Service_State
        buffer.readInt(); // Preparation_State
        buffer.readInt(); // Treatment_State
        buffer.readInt(); // Postprocess_States
        buffer.readInt(); // Disinfection_State
        buffer.readBoolean(); // StartupTestPassed
        buffer.readBoolean(); // T1TestPassed
        buffer.readBoolean(); // SystemExecutionFinished
        buffer.readDouble(); // CL_BALANCE_PP_WASTE_Y
        buffer.readDouble(); // CL_BALANCE_PP_WASTE_E
        buffer.readDouble(); // CL_BALANCE_PP_WASTE_DEM
        buffer.readLong(); // PatientTreatmentBeginn1
        buffer.readLong(); // PatientTreatmentBeginn2
        buffer.readDouble(); // PatientTreatmentDuration
        buffer.readDouble(); // C_UF_VOLUME_CD
        buffer.readDouble(); // C_RO_VOLUME_CD
        buffer.readDouble(); // C_WASTE_VOLUME_CD
        buffer.readDouble(); // C_ACID_VOLUME_CD
        buffer.readDouble(); // C_BASE_VOLUME_CD
        buffer.readDouble(); // C_BLOOD_VOLUME_CD
        buffer.readDouble(); // C_EFF_BLOOD_VOLUME_CD
        buffer.readDouble(); // C_PRE_VOLUME_CD
        buffer.readDouble(); // C_RO_VOLUME_FLOW_CD
        buffer.readDouble(); // CL_HEATER_HW_HMI
        buffer.readDouble(); // CL_HEATER_HW_Y
        buffer.readDouble(); // CL_HEATER_BASE_Y
        buffer.readDouble(); // CL_PH_ACID_Y
        buffer.readDouble(); // CL_PH_BASE_Y
        buffer.readDouble(); // CL_PH_RES_HMI
        buffer.readDouble(); // CL_PH_RES_Y
        buffer.readDouble(); // CL_SUPPLY_FLOW_HMI
        buffer.readDouble(); // CL_FLOW_BLOOD_PUMP_HMI
        buffer.readDouble(); // CL_FLOW_PRE_DILUTION_PUMP_HMI
        buffer.readDouble(); // CL_FLOW_UF_PUMP_HMI
        buffer.readLong(); // ErrH_BloodPumpStoppedTime1
        buffer.readLong(); // ErrH_BloodPumpStoppedTime2
        buffer.readDouble(); // FlowDialyzer
        buffer.readDouble(); // PhAcid
        buffer.readDouble(); // PhBase
        buffer.readDouble(); // HeaterBase
        buffer.readBoolean(); // TreatmentExpertModeActive
        buffer.readBoolean(); // TreatmentPhAutomaticState
        buffer.readBoolean(); // TreatmentTemperatureAutomaticState
        buffer.readBoolean(); // AutomaticVolumeResetExecuted
        buffer.readDouble(); // CL_PH_BASE_HMI
        buffer.readDouble(); // CL_PH_ACID_HMI
        buffer.readDouble(); // CL_FLOW_DIA_HMI
        buffer.readDouble(); // CL_HEATER_BASE_HMI
        buffer.readDouble(); // CL_PERCENTAGE_HEATER_BASE_HMI
        buffer.readDouble(); // CL_PERCENTAGE_HEATER_HW_HMI
        buffer.readDouble(); // CL_FLOW_HW_ACID_HMI
        buffer.readDouble(); // CL_FLOW_HW_BASE_HMI
        buffer.readDouble(); // pH_AutomaticStartTimer
        buffer.readDouble(); // CL_PH_RES_Y_MEAN
        buffer.readDouble(); // CL_PH_RES_Y_RANGE
        buffer.readDouble(); // NextCrossswitch
        buffer.readBoolean(); // FirstCrossswitchExecuted
        buffer.readBoolean(); // TreatmentPredilutionActive
        buffer.readBoolean(); // InitialStartAutomaticComplete
        buffer.readDouble(); // HW_ACID_FLOW_MEAN
        buffer.readDouble(); // HW_BASE_FLOW_MEAN
        buffer.readBoolean(); // TreatmentFirstPatientConnectionComplete
        buffer.readByte(); // RemainingConcentrateErrorResets
        buffer.readInt(); // Treatment_Mode
        buffer.readLong(); // CirculationBegin1
        buffer.readLong(); // CirculationBegin2
        buffer.readDouble(); // CirculationDuration
        buffer.readBoolean(); // TreatmentStartHepaWashForTheFirstTime
        buffer.readBoolean(); // Init2_Selected
        buffer.readDouble(); // C_SUPPLY_VOLUME_CD
        buffer.readDouble(); // C_UF_PATIENT_VOLUME_CD
        buffer.readDouble(); // C_UF_ADDITIONS_VOLUME_CD
        buffer.readDouble(); // CL_FLOW_UF_PATIENT_HMI
        buffer.readDouble(); // CL_FLOW_UF_ADDITIONS_HMI


    }
}
