package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.nio.charset.Charset;

public class HCSystemDiagnosis implements BinaryLabviewData {

    // Cluster #2
    // #1 DBL #9
    double ProcessorLoad;
    double ProcessorLoad_Max;
    double ProcessorLoad_Min;
    double MemoryUsage;
    double MemoryUsage_Max;
    double MemoryUsage_Min;
    double SystemLoopProcessingTime;
    double SystemLoopProcessingTime_Max;
    double SystemLoopProcessingTime_Min;
    // U64
    long LoopLateCounter;
    // #2 String #4
    String MachineNumber;
    String HostVersion;
    String ControlVersion;
    String ParameterFile;


    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // ProcessorLoad
        buffer.readDouble(); // ProcessorLoad_Max
        buffer.readDouble(); // ProcessorLoad_Min
        buffer.readDouble(); // MemoryUsage
        buffer.readDouble(); // MemoryUsage_Max
        buffer.readDouble(); // MemoryUsage_Min
        buffer.readDouble(); // SystemLoopProcessingTime
        buffer.readDouble(); // SystemLoopProcessingTime_Max
        buffer.readDouble(); // SystemLoopProcessingTime_Min
        buffer.readLong(); // LoopLateCounter
        int size1 = buffer.readInt();
        MachineNumber= (String) buffer.readCharSequence(size1, Charset.defaultCharset()); // MachineNumber
        int size2 = buffer.readInt();
        HostVersion= (String) buffer.readCharSequence(size2, Charset.defaultCharset()); // HostVersion
        int size3 = buffer.readInt();
        ControlVersion= (String) buffer.readCharSequence(size3, Charset.defaultCharset()); // ControlVersion
        int size4 = buffer.readInt();
        ParameterFile= (String) buffer.readCharSequence(size4, Charset.defaultCharset()); // ParameterFile

    }

    @Override
    public String toString() {
        return "HCSystemDiagnosis{" +
                "MachineNumber='" + MachineNumber + '\'' +
                ", HostVersion='" + HostVersion + '\'' +
                ", ControlVersion='" + ControlVersion + '\'' +
                ", ParameterFile='" + ParameterFile + '\'' +
                '}';
    }
}
