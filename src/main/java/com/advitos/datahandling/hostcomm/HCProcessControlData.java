package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.util.List;

public class HCProcessControlData implements BinaryLabviewData {

    // EB #8
    int System_State;
    int Idle_State;
    int Startup_State;
    int Service_State;
    int Preparation_State;
    int Treatment_State;
    int Postprocess_States;
    int Disinfection_State;
    // Boolean #4
    boolean TubeInsertionPreDilutionPump;
    boolean TubeInsertionBloodPump;
    boolean SequenceInit;
    boolean SequenceStop;
    // I32
    int SequencePointer;
    // DBL
    double RemainingSequenceTime;
    // EB #2
    int Active_Subsequence;
    int Subsequence_State;
    // Boolean #3
    boolean SubsequencePause;
    boolean SubsequenceUserPause;
    boolean SubsequenceReset;
    // EB
    int Sequence_State;
    // Array
    List<Integer> activeSequences;
    // Boolean
    boolean StartupTestPassed;
    // EB #11
    int Config_File_State;
    int Parameters_File_State;
    int Warmstart_File_State;
    int pHSondReference_File_State;
    int CitricDisinfection_File_State;
    int SodaDisinfection_File_State;
    int SporotalDisinfection_File_State;
    int AsynchronousSaveData_File_State;
    int PumpOperatingData_File_State;
    int Checksum_File_State;
    int UserData_File_State;
    // Cluster #3
    long getTimestamp3;
    long getTimestamp4;
    int Disinfection_Sequence1;
    byte RemainingRepeatedDisinfections;
    //
    long getTimestamp5;
    long getTimestamp6;
    int Disinfection_Sequence2;
    //
    long getTimestamp7;
    long getTimestamp8;
    int Disinfection_Sequence3;
    // Boolean #2
    boolean T1TestPassed;
    boolean SystemExecutionFinished;
    // DBL
    double CrossswitchInterval;
    // Timestamp
    long LastCrossswitch1;
    long LastCrossswitch2;
    // DBL #2
    double TubesetBloodFillingDuration;
    double BloodPumpRunningDuration;
    // Timestamp
    long PatientTreatmentBeginn1;
    long PatientTreatmentBeginn2;
    // DBL #5
    double PatientTreatmentDuration;
    double FlowDialyzer;
    double PhAcid;
    double PhBase;
    double HeaterBase;
    // Boolean #4
    boolean TreatmentExpertModeActive;
    boolean TreatmentPhAutomaticState;
    boolean TreatmentTemperatureAutomaticState;
    boolean AutomaticVolumeResetExecuted;
    // Cluster: Boolean #7
    boolean HOST_DATA_SAVING_ERROR;
    boolean pH_ACID_SOND_ERROR;
    boolean pH_BASE_SOND_ERROR;
    boolean pH_MONITOR_SOND_ERROR;
    boolean pH_RESERVOIR_SOND_ERROR;
    boolean BLD_ERROR;
    boolean Pending_Service_Verification;
    // Boolean
    boolean ForceStandardOrSporotalDisinfection;
    // EB
    int Container_Movement_State;
    // Boolean #2
    boolean TreatmentStartHepaWash;
    boolean RemoveAirPreDilutionPump;
    // DBL #2
    double CrossswitchDelayTime;
    double NextCrossswitch;
    // Boolean #2
    boolean FirstCrossswitchExecuted;
    boolean PhResAutomatic;
    // EB
    int User_Level;
    // DBL #3
    double AcidDensity;
    double BaseDensity;
    double ScaleCalibrationWeight;
    // EW
    int HydraulicState;
    // Boolean #2
    boolean TreatmentPredilutionActive;
    boolean DynamicPressurePaused;
    // DBL #5
    double InitDoublePhResAutomaticReaction;
    double ReinitDoublePhResAutomaticReaction;
    double UpperLimitationPhResAutomatic;
    double LowerLimitationPhResAutomatic;
    double TimeLimitationPhResAutomatic;
    // Boolean
    boolean TreatmentFirstPatientConnectionComplete;
    // EB
    int Treatment_Mode;
    // Timestamp
    long CirculationBegin1;
    long CirculationBegin2;
    // DBL
    double CirculationDuration;
    // DBL #5
    boolean PossibleToConnectThePatient;
    boolean ForcedToSwitchBackToBypass;
    boolean TreatmentStartHepaWashForTheFirstTime;
    boolean EnableAutomaticSystemStateTransitionFromStartupToIdle;
    boolean DisconnectPatientWithoutReinfusion;
    // Cluster #3
    // #1 DBL #7
    double ScaleDeviationCriteria;
    double ScaleDeviationCheckPeriod;
    double PressureCheckCriteria;
    double PressureStabilizationCriteria;
    double PressureDeviationCheckPeriod;
    double OverpressureThreshold;
    double UnderpressureThreshold;
    // #2 DBL #2
    double CL_FLOW_HW_ACID_PROC1;
    double CL_FLOW_HW_BASE_PROC1;
    // #3 DBL #2
    double CL_FLOW_HW_ACID_PROC2;
    double CL_FLOW_HW_BASE_PROC2;
    // Boolean
    boolean ReinfusionArtBloodMonitorState;
    // Cluster DBL #2
    double CL_FLOW_HW_ACID_PROC3;
    double CL_FLOW_HW_BASE_PROC3;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // System_State
        buffer.readInt(); // Idle_State
        buffer.readInt(); // Startup_State
        buffer.readInt(); // Service_State
        buffer.readInt(); // Preparation_State
        buffer.readInt(); // Treatment_State
        buffer.readInt(); // Postprocess_States
        buffer.readInt(); // Disinfection_State
        buffer.readBoolean(); // TubeInsertionPreDilutionPump
        buffer.readBoolean(); // TubeInsertionBloodPump
        buffer.readBoolean(); // SequenceInit
        buffer.readBoolean(); // SequenceStop
        buffer.readInt(); // SequencePointer
        buffer.readDouble(); // RemainingSequenceTime
        buffer.readInt(); // Active_Subsequence
        buffer.readInt(); // Subsequence_State
        buffer.readBoolean(); // SubsequencePause
        buffer.readBoolean(); // SubsequenceUserPause
        buffer.readBoolean(); // SubsequenceReset
        buffer.readInt(); // Sequence_State
        int size = buffer.readInt();
        for (int i = 0; i < size; i++) {
            buffer.readInt();
        } // activeSequences
        buffer.readBoolean(); // StartupTestPassed
        buffer.readInt(); // Config_File_State
        buffer.readInt(); // Parameters_File_State
        buffer.readInt(); // Warmstart_File_State
        buffer.readInt(); // pHSondReference_File_State
        buffer.readInt(); // CitricDisinfection_File_State
        buffer.readInt(); // SodaDisinfection_File_State
        buffer.readInt(); // SporotalDisinfection_File_State
        buffer.readInt(); // AsynchronousSaveData_File_State
        buffer.readInt(); // PumpOperatingData_File_State
        buffer.readInt(); // Checksum_File_State
        buffer.readInt(); // UserData_File_State
        buffer.readLong(); // getTimestamp3
        buffer.readLong(); // getTimestamp4
        buffer.readInt(); // Disinfection_Sequence1
        buffer.readByte(); // RemainingRepeatedDisinfections
        buffer.readLong(); // getTimestamp5
        buffer.readLong(); // getTimestamp6
        buffer.readInt(); // Disinfection_Sequence2
        buffer.readLong(); // getTimestamp7
        buffer.readLong(); // getTimestamp8
        buffer.readInt(); // Disinfection_Sequence3
        buffer.readBoolean(); // T1TestPassed
        buffer.readBoolean(); // SystemExecutionFinished
        buffer.readDouble(); // CrossswitchInterval
        buffer.readLong(); // LastCrossswitch1
        buffer.readLong(); // LastCrossswitch2
        buffer.readDouble(); // TubesetBloodFillingDuration
        buffer.readDouble(); // BloodPumpRunningDuration
        buffer.readLong(); // PatientTreatmentBeginn1
        buffer.readLong(); // PatientTreatmentBeginn2
        buffer.readDouble(); // PatientTreatmentDuration
        buffer.readDouble(); // FlowDialyzer
        buffer.readDouble(); // PhAcid
        buffer.readDouble(); // PhBase
        buffer.readDouble(); // HeaterBase
        buffer.readBoolean(); // TreatmentExpertModeActive
        buffer.readBoolean(); // TreatmentPhAutomaticState
        buffer.readBoolean(); // TreatmentTemperatureAutomaticState
        buffer.readBoolean(); // AutomaticVolumeResetExecuted
        buffer.readBoolean(); // HOST_DATA_SAVING_ERROR
        buffer.readBoolean(); // pH_ACID_SOND_ERROR
        buffer.readBoolean(); // pH_BASE_SOND_ERROR
        buffer.readBoolean(); // pH_MONITOR_SOND_ERROR
        buffer.readBoolean(); // pH_RESERVOIR_SOND_ERROR
        buffer.readBoolean(); // BLD_ERROR
        buffer.readBoolean(); // Pending_Service_Verification
        buffer.readBoolean(); // ForceStandardOrSporotalDisinfection
        buffer.readInt(); // Container_Movement_State
        buffer.readBoolean(); // TreatmentStartHepaWash
        buffer.readBoolean(); // RemoveAirPreDilutionPump
        buffer.readDouble(); // CrossswitchDelayTime
        buffer.readDouble(); // NextCrossswitch
        buffer.readBoolean(); // FirstCrossswitchExecuted
        buffer.readBoolean(); // PhResAutomatic
        buffer.readInt(); // User_Level
        buffer.readDouble(); // AcidDensity
        buffer.readDouble(); // BaseDensity
        buffer.readDouble(); // ScaleCalibrationWeight
        buffer.readInt(); // HydraulicState
        buffer.readBoolean(); // TreatmentPredilutionActive
        buffer.readBoolean(); // DynamicPressurePaused
        buffer.readDouble(); // InitDoublePhResAutomaticReaction
        buffer.readDouble(); // ReinitDoublePhResAutomaticReaction
        buffer.readDouble(); // UpperLimitationPhResAutomatic
        buffer.readDouble(); // LowerLimitationPhResAutomatic
        buffer.readDouble(); // TimeLimitationPhResAutomatic
        buffer.readBoolean(); // TreatmentFirstPatientConnectionComplete
        buffer.readInt(); // Treatment_Mode
        buffer.readLong(); // CirculationBegin1
        buffer.readLong(); // CirculationBegin2
        buffer.readDouble(); // CirculationDuration
        buffer.readBoolean(); // PossibleToConnectThePatient
        buffer.readBoolean(); // ForcedToSwitchBackToBypass
        buffer.readBoolean(); // TreatmentStartHepaWashForTheFirstTime
        buffer.readBoolean(); // EnableAutomaticSystemStateTransitionFromStartupToIdle
        buffer.readBoolean(); // DisconnectPatientWithoutReinfusion
        buffer.readDouble(); // ScaleDeviationCriteria
        buffer.readDouble(); // ScaleDeviationCheckPeriod
        buffer.readDouble(); // PressureCheckCriteria
        buffer.readDouble(); // PressureStabilizationCriteria
        buffer.readDouble(); // PressureDeviationCheckPeriod
        buffer.readDouble(); // OverpressureThreshold
        buffer.readDouble(); // UnderpressureThreshold
        buffer.readDouble(); // CL_FLOW_HW_ACID_PROC1
        buffer.readDouble(); // CL_FLOW_HW_BASE_PROC1
        buffer.readDouble(); // CL_FLOW_HW_ACID_PROC2
        buffer.readDouble(); // CL_FLOW_HW_BASE_PROC2
        buffer.readBoolean(); // ReinfusionArtBloodMonitorState
        buffer.readDouble(); // CL_FLOW_HW_ACID_PROC3
        buffer.readDouble(); // CL_FLOW_HW_BASE_PROC3

    }
}
