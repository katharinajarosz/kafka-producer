package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCNumericOutput implements BinaryLabviewData {

    int numericOutput;
    boolean err;
    double dem;
    double cd;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // numericOutput
        buffer.readBoolean(); // err
        buffer.readDouble(); // dem
        buffer.readDouble(); // cd

    }
}
