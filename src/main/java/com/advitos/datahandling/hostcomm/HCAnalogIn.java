package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCAnalogIn implements BinaryLabviewData {

    double f;
    double cd;
    double sc_of;
    double sc_g;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // f
        buffer.readDouble(); // cd
        buffer.readDouble(); // sc_of
        buffer.readDouble(); // sc_g

    }
}
