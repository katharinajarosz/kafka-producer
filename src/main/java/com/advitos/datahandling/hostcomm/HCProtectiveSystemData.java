package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.util.List;

public class HCProtectiveSystemData implements BinaryLabviewData {

    // Boolean #3
    boolean SondCalDataRead;
    boolean AnalogScalingDataRead;
    boolean ConnectionState;
    // U32 #3
    int UnknownMessageCount;
    int CRC_ErrorCount;
    int Message_Counter_ErrorCount;
    // U8 #5
    byte SendMSGCounter;
    byte ProtectState;
    byte ProtectSubstate;
    byte ControlStatusByte;
    byte ProtectStatusByte;
    // Cluster: Boolean #7
    boolean Heater;
    boolean Power;
    boolean Bypass;
    boolean TestSync;
    boolean ControlMuted;
    boolean ScalesSync;
    boolean ActiveFailure;
    // Array
    List<Boolean> ErrorBit;
    // Cluster #16
    // #1-4 SondData.class
    List<HCSondData> sondData;
    // #5-6 PumpData.class
    List<HCPumpData> pumpData;
    // #7-16 AnalogIn.class
    List<HCAnalogIn> analogIns;
    // U8 #4
    byte LastReceivedMsgID;
    byte LastSendMsgID;
    byte LastSendMainState;
    byte LastSendSubState;
    // U32
    int RS232CommunicationLoopExecutionTime;
    // Cluster #2
    HCBalancingData balancingData = new HCBalancingData();

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readBoolean(); // SondCalDataRead
        buffer.readBoolean(); // AnalogScalingDataRead
        buffer.readBoolean(); // ConnectionState
        buffer.readInt(); // UnknownMessageCount
        buffer.readInt(); // CRC_ErrorCount
        buffer.readInt(); // Message_Counter_ErrorCount
        buffer.readByte(); // SendMSGCounter
        buffer.readByte(); // ProtectState
        buffer.readByte(); // ProtectSubstate
        buffer.readByte(); // ControlStatusByte
        buffer.readByte(); // ProtectStatusByte
        buffer.readBoolean(); // Heater
        buffer.readBoolean(); // Power
        buffer.readBoolean(); // Bypass
        buffer.readBoolean(); // TestSync
        buffer.readBoolean(); // ControlMuted
        buffer.readBoolean(); // ScalesSync
        buffer.readBoolean(); // ActiveFailure
        int size = buffer.readInt();
        for (int i = 0; i < size; i++) {
            buffer.readBoolean();
        } // ErrorBit
        for (int i = 0; i < 4; i++) {
            HCSondData sd = new HCSondData();
            sd.parse(buffer);
        } // sondData
        for (int i = 0; i < 2; i++) {
            HCPumpData pd = new HCPumpData();
            pd.parse(buffer);
        } // pumpData
        for (int i = 0; i < 10; i++) {
            HCAnalogIn ai = new HCAnalogIn();
            ai.parse(buffer);
        } // analogIns
        buffer.readByte(); // LastReceivedMsgID
        buffer.readByte(); // LastSendMsgID
        buffer.readByte(); // LastSendMainState
        buffer.readByte(); // LastSendSubState
        buffer.readInt(); // RS232CommunicationLoopExecutionTime
        balancingData.parse(buffer);

    }
}
