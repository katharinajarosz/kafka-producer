package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import com.advitos.datahandling.labviewclasses.ErrorElement;
import io.netty.buffer.ByteBuf;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class HCErrorData implements BinaryLabviewData {

    // Array
    private List<ErrorElement> errorElements = new ArrayList<>();
    // EB
    int Buzzer_Mute_State;
    // Boolean #2
    boolean ConfigurationMD5CheckState;
    boolean ParametersMD5CheckState;
    // Timestamp
    long BloodPumpStoppedTime1;
    long BloodPumpStoppedTime2;
    // String #3
    String RTexeMD5calculated;
    String RTexeMD5File;
    String RTiniMD5;
    // Boolean #2
    boolean RTiniMD5CheckState;
    boolean VenousOverpressure;
    // Cluster #4
    // #1-3 PressureLimit.class
    List<PressureLimit> pressureLimits;
    // #4 PhReservoirRangeCheck.class
    PhReservoirRangeCheck phReservoirRangeCheck = new PhReservoirRangeCheck();
    // DBL #2
    double ReservoirCheckSymmetricLimit;
    double ReservoirCheckTimeLimit;
    // Boolean
    boolean ForceContainerToMoveDown;
    // U8
    byte RemainingConcentrateErrorResets;
    // Cluster #2
    // #1 DBL #2
    double UpperThreshold;
    double LowerThreshold;
    // #2 DBL #4
    double ContainerMaximumWeightLimit;
    double ContainerLiftMotorBlockDetectionThreshold1;
    double ContainerLiftMotorBlockDetectionThreshold2;
    double ContainerLiftMotorBlockDetectionThreshold3;
    // DBL
    double WasteOverPressureThreshold;

    @Override
    public void parse(ByteBuf buffer) {
        int arraySize = buffer.readInt();
        for (int i = 0; i < arraySize; i++) {
            ErrorElement e = new ErrorElement();
            e.parse(buffer);
        } // errorElements
        buffer.readInt(); // Buzzer_Mute_State
        buffer.readBoolean(); // ConfigurationMD5CheckState
        buffer.readBoolean(); // ParametersMD5CheckState
        buffer.readLong(); // BloodPumpStoppedTime1
        buffer.readLong(); // BloodPumpStoppedTime2
        int size1 = buffer.readInt();
        buffer.readCharSequence(size1, Charset.defaultCharset()); // RTexeMD5calculated
        int size2 = buffer.readInt();
        buffer.readCharSequence(size2, Charset.defaultCharset()); // RTexeMD5File
        int size3 = buffer.readInt();
        buffer.readCharSequence(size3, Charset.defaultCharset()); // RTiniMD5
        buffer.readBoolean(); // RTiniMD5CheckState
        buffer.readBoolean(); // VenousOverpressure
        for (int i = 0; i < 3; i++) {
            PressureLimit pl = new PressureLimit();
            pl.parse(buffer);
        } // pressureLimits
        phReservoirRangeCheck.parse(buffer);
        buffer.readDouble(); // ReservoirCheckSymmetricLimit
        buffer.readDouble(); // ReservoirCheckTimeLimit
        buffer.readBoolean(); // ForceContainerToMoveDown
        buffer.readByte(); // RemainingConcentrateErrorResets
        buffer.readDouble(); // UpperThreshold
        buffer.readDouble(); // LowerThreshold
        buffer.readDouble(); // ContainerMaximumWeightLimit
        buffer.readDouble(); // ContainerLiftMotorBlockDetectionThreshold1
        buffer.readDouble(); // ContainerLiftMotorBlockDetectionThreshold2
        buffer.readDouble(); // ContainerLiftMotorBlockDetectionThreshold3
        buffer.readDouble(); // WasteOverPressureThreshold

    }
}
