package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class BalancingZScore implements BinaryLabviewData {

    double treshold;
    double influence;
    short lag;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // treshold
        buffer.readDouble(); // influence
        buffer.readShort(); // lag

    }
}
