package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCDigitalInput implements BinaryLabviewData {

    private int digitalInput;
    private boolean r;
    private boolean cd;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // digitalInput
        buffer.readBoolean(); // r
        buffer.readBoolean(); // cd
    }
}
