package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.util.List;

public class HCProcessImage implements BinaryLabviewData {

    // EW
    int Cmd;
    // Cluster #2
    // #1 EW
    int BloodPumpButtonEventType;
    // #2 Array #5
    List<HCNumericInput> numericInputs;
    List<HCDigitalInput> digitalInputs;
    List<HCClosedLoop> closedLoops;
    List<HCNumericOutput> numericOutputs;
    List<HCDigitalOutput> digitalOutputs;
    // Cluster #9
    // #1 IOData.class
    HCIOData ioData = new HCIOData();
    // #2 ProcessControlDta.class
    HCProcessControlData processControlData = new HCProcessControlData();
    // #3 ProtectiveSystemData.class
    HCProtectiveSystemData protectiveSystemData = new HCProtectiveSystemData();
    // #4 SystemDiagnosis.class
    HCSystemDiagnosis systemDiagnosis = new HCSystemDiagnosis();
    // #5 ErrorData.class
    HCErrorData errorData = new HCErrorData();
    // #6 Cluster #4 SondData.class
    List<HCSondData> sondReferenceData;
    // #7 WarmResetData.class
    WarmResetData warmResetData = new WarmResetData();
    // #8-9 Scale.class
    List<Scale> scales;


    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // Cmd
        buffer.readInt(); // BloodPumpButtonEventType

        int size1 = buffer.readInt();
        for (int i = 0; i < size1; i++) {
            HCNumericInput ni = new HCNumericInput();
            ni.parse(buffer);
        } // numericInputs

        int size2 = buffer.readInt();
        for (int i = 0; i < size2; i++) {
            HCDigitalInput di = new HCDigitalInput();
            di.parse(buffer);
        } // digitalInputs

        int size3 = buffer.readInt();
        for (int i = 0; i < size3; i++) {
            HCClosedLoop cl = new HCClosedLoop();
            cl.parse(buffer);
        } // closedLoops

        int size4 = buffer.readInt();
        for (int i = 0; i < size4; i++) {
            HCNumericOutput no = new HCNumericOutput();
            no.parse(buffer);
        } // numericOutputs

        int size5 = buffer.readInt();
        for (int i = 0; i < size5; i++) {
            HCDigitalOutput out = new HCDigitalOutput();
            out.parse(buffer);
        } // digitalOutputs

        ioData.parse(buffer);
        processControlData.parse(buffer);
        protectiveSystemData.parse(buffer);
        systemDiagnosis.parse(buffer);
        errorData.parse(buffer);

        for (int i = 0; i < 4; i++) {
            HCSondData sd = new HCSondData();
            sd.parse(buffer);
        } // sondReferenceData

        warmResetData.parse(buffer);

        for (int i = 0; i < 2; i++) {
            Scale s = new Scale();
            s.parse(buffer);
        } // scales

    }
}
