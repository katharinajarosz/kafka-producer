package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.nio.charset.Charset;

public class HCBalancingData implements BinaryLabviewData {

    // DBL #14
    double ufHost;
    double ufControl;
    double ufWeight;
    double acidFlowConrol;
    double acidWeight;
    double acidDensity;
    double baseFlowControl;
    double baseWeight;
    double baseDensity;
    double scale1row;
    double scale2row;
    double scaleDeviation;
    double scaleSumCurrent;
    double balanceOffset;
    // Cluster String
    String ProtectiveSystemVersion;
    // Boolean
    boolean ProtectiveSystemVersionCompatibility;
    // String
    String PumpSystemVersion;
    // Boolean
    boolean PumpSystemVersionCompatibility;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // ufHost
        buffer.readDouble(); // ufControl
        buffer.readDouble(); // ufWeight
        buffer.readDouble(); // acidFlowConrol
        buffer.readDouble(); // acidWeight
        buffer.readDouble(); // acidDensity
        buffer.readDouble(); // baseFlowControl
        buffer.readDouble(); // baseWeight
        buffer.readDouble(); // baseDensity
        buffer.readDouble(); // scale1row
        buffer.readDouble(); // scale2row
        buffer.readDouble(); // scaleDeviation
        buffer.readDouble(); // scaleSumCurrent
        buffer.readDouble(); // balanceOffset
        int size1 = buffer.readInt();
        buffer.readCharSequence(size1, Charset.defaultCharset()); // ProtectiveSystemVersion
        buffer.readBoolean(); // ProtectiveSystemVersionCompatibility
        int size2 = buffer.readInt();
        buffer.readCharSequence(size2, Charset.defaultCharset()); // PumpSystemVersion
        buffer.readBoolean(); // PumpSystemVersionCompatibility

    }
}
