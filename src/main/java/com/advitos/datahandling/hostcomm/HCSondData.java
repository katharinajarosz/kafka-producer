package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCSondData implements BinaryLabviewData {

    int Serial_Number;
    short pH_Buffer_1;
    short pH_Buffer_2;
    short pH_Voltage_1;
    short pH_Voltage_2;
    short Temperature_1;
    short Temperature_2;
    short WEAR;
    short UpH;
    short UTemp;
    double U_of;
    double sPercent;
    double Temperature;
    double pH;
    int ReadErrorCounter;
    boolean ReadError;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // Serial_Number
        buffer.readShort(); // pH_Buffer_1
        buffer.readShort(); // pH_Buffer_2
        buffer.readShort(); // pH_Voltage_1
        buffer.readShort(); // pH_Voltage_2
        buffer.readShort(); // Temperature_1
        buffer.readShort(); // Temperature_2
        buffer.readShort(); // WEAR
        buffer.readShort(); // UpH
        buffer.readShort(); // UTemp
        buffer.readDouble(); // U_of
        buffer.readDouble(); // sPercent
        buffer.readDouble(); // Temperature
        buffer.readDouble(); // pH
        buffer.readInt(); // ReadErrorCounter
        buffer.readBoolean(); // ReadError

    }
}
