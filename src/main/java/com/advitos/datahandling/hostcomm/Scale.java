package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.nio.charset.Charset;

public class Scale implements BinaryLabviewData {

    // DBL
    double Value;
    // DBL #7
    boolean GN_Overflow;
    boolean GN_Underflow;
    boolean GN_ParseError;
    boolean Connected;
    boolean SetZeroSuccess;
    boolean SetGainSuccess;
    boolean SaveCalSuccess;
    // DBL
    double CalibrationWeight;
    // I8 #2
    byte FF;
    byte FL;
    // DBL
    double CG;
    // String #2
    String ID;
    String IV;
    // I8
    byte UR;
    // I16
    short DS;
    // I8
    byte DP;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // Value
        buffer.readBoolean(); // GN_Overflow
        buffer.readBoolean(); // GN_Underflow
        buffer.readBoolean(); // GN_ParseError
        buffer.readBoolean(); // Connected
        buffer.readBoolean(); // SetZeroSuccess
        buffer.readBoolean(); // SetGainSuccess
        buffer.readBoolean(); // SaveCalSuccess
        buffer.readDouble(); // CalibrationWeight
        buffer.readByte(); // FF
        buffer.readByte(); // FL
        buffer.readDouble(); // CG
        int size1 = buffer.readInt();
        buffer.readCharSequence(size1, Charset.defaultCharset()); // ID
        int size2 = buffer.readInt();
        buffer.readCharSequence(size2, Charset.defaultCharset()); // IV
        buffer.readByte(); // UR
        buffer.readShort(); // DS
        buffer.readByte(); // DP

    }
}
