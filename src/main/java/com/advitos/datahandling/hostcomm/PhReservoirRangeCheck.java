package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class PhReservoirRangeCheck implements BinaryLabviewData {

    double MEAS_SMOOTHED;
    long DEM_Increased1;
    long DEM_Increased2;
    long DEM_Decreased1;
    long DEM_Decreased2;
    double Relative_UL;
    double Relative_LL;
    double Absolute_UL;
    double Absolute_LL;
    boolean RangeActive;
    double RangeCheckDelayTime;
    double IncreaseDelay;
    double DecreaseDelay;
    double SmoothTime;


    @Override
    public void parse(ByteBuf buffer) {
        buffer.readDouble(); // MEAS_SMOOTHED
        buffer.readLong(); // DEM_Increased1
        buffer.readLong(); // DEM_Increased2
        buffer.readLong(); // DEM_Decreased1
        buffer.readLong(); // DEM_Decreased2
        buffer.readDouble(); // Relative_UL
        buffer.readDouble(); // Relative_LL
        buffer.readDouble(); // Absolute_UL
        buffer.readDouble(); // Absolute_LL
        buffer.readBoolean(); // RangeActive
        buffer.readDouble(); // RangeCheckDelayTime
        buffer.readDouble(); // IncreaseDelay
        buffer.readDouble(); // DecreaseDelay
        buffer.readDouble(); // SmoothTime

    }
}
