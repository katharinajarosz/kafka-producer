package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class PressureLimit implements BinaryLabviewData {

    boolean LimitActive;
    double Relative_LL;
    double Relative_UL;
    double Absolute_LL;
    double Absolute_UL;
    boolean RecalculateAbsoluteLimits;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readBoolean(); // LimitActive
        buffer.readDouble(); // Relative_LL
        buffer.readDouble(); // Relative_UL
        buffer.readDouble(); // Absolute_LL
        buffer.readDouble(); // Absolute_UL
        buffer.readBoolean(); // RecalculateAbsoluteLimits

    }
}
