package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCNumericInput implements BinaryLabviewData {

    int numericInput;
    double r;
    double r1;
    double tnf;
    int mean;
    double f;
    double sc_of;
    double sc_g;
    double cd;
    int es_mean;
    double es_cd;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // numericInput
        buffer.readDouble(); // r
        buffer.readDouble(); // r1
        buffer.readDouble(); // tnf
        buffer.readInt(); // mean
        buffer.readDouble(); // f
        buffer.readDouble(); // sc_of
        buffer.readDouble(); // sc_g
        buffer.readDouble(); // cd
        buffer.readInt(); // es_mean
        buffer.readDouble(); // es_cd

    }
}
