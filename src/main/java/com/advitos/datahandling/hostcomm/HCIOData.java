package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

import java.util.List;

public class HCIOData implements BinaryLabviewData {

    long timestamp1;
    long timestamp2;
    // EB #16
    int ppSupplyModeHMI;
    int ppSupplyModePROC;
    int ppSupplyModeDEM;
    int ppSupplyModeDEM1;
    int ppWasteModeHMI;
    int ppWasteModePROC;
    int ppWasteModeDMI;
    int ppWasteModeDMI1;
    int gpModeHMI;
    int gpModePROC;
    int gpModeDMI;
    int gpModeDMI1;
    int heaterModeHMI;
    int heaterModePROC;
    int heaterModeDMI;
    int heaterModeDMI1;
    // Boolean #1
    boolean usePreparationHightConcRatio;
    // DBL #10
    double CL_BALANCE_PP_WASTE_Y;
    double CL_BALANCE_PP_WASTE_E;
    double CL_PH_ACID_Y;
    double CL_PH_BASE_Y;
    double CL_PH_RES_Y;
    double CL_HEATER_HW_Y;
    double CL_HEATER_BASE_Y;
    double CL_PH_RES_Y_MEAN;
    double CL_PH_RES_Y_RANGE;
    double pH_AutomaticStartTimer;
    // Array #12
    List<Byte> ActiveAcidMode;
    List<Double> ActiveAcidYMinStart;
    List<Double> ActiveAcidYMinStop;
    List<Double> ActiveAcidYMaxStart;
    List<Double> ActiveAcidYMaxStop;
    List<Double> ActiveAcidDuration;
    List<Byte> ActiveBaseMode;
    List<Double> ActiveBaseYMinStart;
    List<Double> ActiveBaseYMinStop;
    List<Double> ActiveBaseYMaxStart;
    List<Double> ActiveBaseYMaxStop;
    List<Double> ActiveBaseDuration;
    // DBL
    double InitSwitch_PH_Threshold;
    // Boolean
    boolean Init2_Selected;
    // Array #12
    List<Byte> Init1_AcidMode;
    List<Double> Init1_AcidYMinStart;
    List<Double> Init1_AcidYMinStop;
    List<Double> Init1_AcidYMaxStart;
    List<Double> Init1_AcidYMaxStop;
    List<Double> Init1_AcidDuration;
    List<Byte> Init1_BaseMode;
    List<Double> Init1_BaseYMinStart;
    List<Double> Init1_BaseYMinStop;
    List<Double> Init1_BaseYMaxStart;
    List<Double> Init1_BaseYMaxStop;
    List<Double> Init1_BaseDuration;
    // DBL #5
    double Init1_Treatment_PH_Acid;
    double Init1_Treatment_PH_Base;
    double Init1_Treatment_Reservoir_Percentage;
    double Init1_Treatment_Reservoir_Ph;
    double Init1_Supply_Flow;
    // Array #12
    List<Byte> Init2_AcidMode;
    List<Double> Init2_AcidYMinStart;
    List<Double> Init2_AcidYMinStop;
    List<Double> Init2_AcidYMaxStart;
    List<Double> Init2_AcidYMaxStop;
    List<Double> Init2_AcidDuration;
    List<Double> Init2_BaseMode;
    List<Double> Init2_BaseYMinStart;
    List<Double> Init2_BaseYMinStop;
    List<Double> Init2_BaseYMaxStart;
    List<Double> Init2_BaseYMaxStop;
    List<Double> Init2_BaseDuration;
    // DBL #5
    double Init2_Treatment_PH_Acid;
    double Init2_Treatment_PH_Base;
    double Init2_Treatment_Reservoir_Percentage;
    double Init2_Treatment_Reservoir_Ph;
    double Init2_Supply_Flow;
    // Array #12
    List<Byte> ReinitAcidMode;
    List<Double> ReinitAcidYMinStart;
    List<Double> ReinitAcidYMinStop;
    List<Double> ReinitAcidYMaxStart;
    List<Double> ReinitAcidYMaxStop;
    List<Double> ReinitAcidDuration;
    List<Byte> ReinitBaseMode;
    List<Double> ReinitBaseYMinStart;
    List<Double> ReinitBaseYMinStop;
    List<Double> ReinitBaseYMaxStart;
    List<Double> ReinitBaseYMaxStop;
    List<Double> ReinitBaseDuration;
    // Boolean
    boolean InitialStartAutomaticComplete;
    // DBL #2
    double HW_ACID_FLOW_MEAN;
    double HW_BASE_FLOW_MEAN;
    // Array
    List<ConcRatioConfiguration> concRatioConfigurationList;
    // Cluster
    BalancingZScore balancingZScore = new BalancingZScore();

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readLong(); // timestamp1
        buffer.readLong(); // timestamp2
        buffer.readInt(); // ppSupplyModeHMI
        buffer.readInt(); // ppSupplyModePROC
        buffer.readInt(); // ppSupplyModeDEM
        buffer.readInt(); // ppSupplyModeDEM1
        buffer.readInt(); // ppWasteModeHMI
        buffer.readInt(); // ppWasteModePROC
        buffer.readInt(); // ppWasteModeDMI
        buffer.readInt(); // ppWasteModeDMI1
        buffer.readInt(); // gpModeHMI
        buffer.readInt(); // gpModePROC
        buffer.readInt(); // gpModeDMI
        buffer.readInt(); // gpModeDMI1
        buffer.readInt(); // heaterModeHMI
        buffer.readInt(); // heaterModePROC
        buffer.readInt(); // heaterModeDMI
        buffer.readInt(); // heaterModeDMI1
        buffer.readBoolean(); // usePreparationHightConcRatio
        buffer.readDouble(); // CL_BALANCE_PP_WASTE_Y
        buffer.readDouble(); // CL_BALANCE_PP_WASTE_E
        buffer.readDouble(); // CL_PH_ACID_Y
        buffer.readDouble(); // CL_PH_BASE_Y
        buffer.readDouble(); // CL_PH_RES_Y
        buffer.readDouble(); // CL_HEATER_HW_Y
        buffer.readDouble(); // CL_HEATER_BASE_Y
        buffer.readDouble(); // CL_PH_RES_Y_MEAN
        buffer.readDouble(); // CL_PH_RES_Y_RANGE
        buffer.readDouble(); // pH_AutomaticStartTimer
        // Array #12
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // ActiveAcidMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveAcidYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveAcidYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveAcidYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveAcidYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveAcidDuration
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // ActiveBaseMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveBaseYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveBaseYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveBaseYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveBaseYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ActiveBaseDuration
        }
        buffer.readDouble(); // InitSwitch_PH_Threshold
        buffer.readBoolean(); // Init2_Selected
        // Array #12
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // Init1_AcidMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_AcidYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_AcidYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_AcidYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_AcidYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_AcidDuration
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // Init1_BaseMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_BaseYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_BaseYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_BaseYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_BaseYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init1_BaseDuration
        }
        buffer.readDouble(); // Init1_Treatment_PH_Acid
        buffer.readDouble(); // Init1_Treatment_PH_Base
        buffer.readDouble(); // Init1_Treatment_Reservoir_Percentage
        buffer.readDouble(); // Init1_Treatment_Reservoir_Ph
        buffer.readDouble(); // Init1_Supply_Flow
        // Array #12
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // Init2_AcidMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_AcidYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_AcidYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_AcidYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_AcidYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_AcidDuration
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_BaseMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_BaseYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_BaseYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_BaseYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_BaseYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // Init2_BaseDuration
        }
        buffer.readDouble(); // Init2_Treatment_PH_Acid
        buffer.readDouble(); // Init2_Treatment_PH_Base
        buffer.readDouble(); // Init2_Treatment_Reservoir_Percentage
        buffer.readDouble(); // Init2_Treatment_Reservoir_Ph
        buffer.readDouble(); // Init2_Supply_Flow
        // Array #12
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // ReinitAcidMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitAcidYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitAcidYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitAcidYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitAcidYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitAcidDuration
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readByte(); // ReinitBaseMode
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitBaseYMinStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitBaseYMinStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitBaseYMaxStart
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitBaseYMaxStop
        }
        for (int i = 0; i < buffer.readInt(); i++) {
            buffer.readDouble(); // ReinitBaseDuration
        }
        buffer.readBoolean(); // InitialStartAutomaticComplete
        buffer.readDouble(); // HW_ACID_FLOW_MEAN
        buffer.readDouble(); // HW_BASE_FLOW_MEAN
        for (int i = 0; i < buffer.readInt(); i++) {
            ConcRatioConfiguration c = new ConcRatioConfiguration();
            c.parse(buffer);
        } // concRatioConfigurationList
        balancingZScore.parse(buffer);

    }
}
