package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCClosedLoop implements BinaryLabviewData {

    int closedLoop;
    double hmi;
    double proc;
    double sc_g;
    double dem;
    double dem_sc;
    double dem_sc_max;
    double dem_sc_min;
    double ramp;
    double xd1;
    double y;
    double ymax;
    double ymin;
    double inp;
    int de_dt_mean;
    double de_dt_tnf;
    int de_dt_t;
    int type;
    double e;
    double de_dt;
    double kp;
    double kd;
    double ki;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // closedLoop
        buffer.readDouble(); // hmi
        buffer.readDouble(); // proc
        buffer.readDouble(); // sc_g
        buffer.readDouble(); // dem
        buffer.readDouble(); // dem_sc
        buffer.readDouble(); // dem_sc_max
        buffer.readDouble(); // dem_sc_min
        buffer.readDouble(); // ramp
        buffer.readDouble(); // xd1
        buffer.readDouble(); // y
        buffer.readDouble(); // ymax
        buffer.readDouble(); // ymin
        buffer.readDouble(); // inp
        buffer.readInt(); // de_dt_mean
        buffer.readDouble(); // de_dt_tnf
        buffer.readInt(); // de_dt_t
        buffer.readInt(); // type
        buffer.readDouble(); // e
        buffer.readDouble(); // de_dt
        buffer.readDouble(); // kp
        buffer.readDouble(); // kd
        buffer.readDouble(); // ki

    }
}
