package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class ConcRatioConfiguration implements BinaryLabviewData {

    int Name;
    double AcidWaterRatio;
    double BaseWaterRatio;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // Name
        buffer.readDouble(); // AcidWaterRatio
        buffer.readDouble(); // BaseWaterRatio

    }
}
