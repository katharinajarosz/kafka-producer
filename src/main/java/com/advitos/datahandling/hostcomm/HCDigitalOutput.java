package com.advitos.datahandling.hostcomm;

import com.advitos.datahandling.BinaryLabviewData;
import io.netty.buffer.ByteBuf;

public class HCDigitalOutput implements BinaryLabviewData {

    int digitalOutput;
    boolean hmi;
    boolean proc;
    boolean err;
    boolean dem;

    @Override
    public void parse(ByteBuf buffer) {
        buffer.readInt(); // digitalOutput
        buffer.readBoolean(); // hmi
        buffer.readBoolean(); // proc
        buffer.readBoolean(); // err
        buffer.readBoolean(); // dem

    }
}
