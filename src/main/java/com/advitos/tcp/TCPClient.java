package com.advitos.tcp;

import com.advitos.kafka.producer.ProcessImageProducer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

@Component
public class TCPClient implements Runnable {

    @Value("${tcpHost}")
    private String ip;
    @Value("${tcpPort}")
    private int port;
    private Socket clientSocket;
    @Autowired
    ProcessImageProducer producer;


    public void run() { // run the service
        try {
            clientSocket = requestServer();
            while (true) {
                ByteBuf bb = getByteBuffersFromServer(clientSocket);
                producer.sendData(bb);
            }
        } catch (IOException ex) {
            System.out.println("--- Interrupt server.NetworkService-run");
        } finally {
            System.out.println("--- Ende server.NetworkService(pool.shutdown)");
            try {
                if (!clientSocket.isClosed()) {
                    System.out.println("--- Ende server.NetworkService:ServerSocket close");
                    clientSocket.close();
                }
                } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    Socket requestServer() throws IOException {
        Socket socket = new Socket(ip, port); // verbindet sich mit Server
        String message = "Client started";
        printMessage(socket, message);
        return socket;

    }

    void printMessage(Socket socket, String message) throws IOException {
        PrintWriter printWriter =
                new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()));
        printWriter.print(message);
        printWriter.flush();
    }

    ByteBuf getByteBuffersFromServer(Socket socket) throws IOException {
        DataInputStream bs = new DataInputStream(socket.getInputStream());
        int size = bs.readInt();
        byte[] bytes = new byte[size];
        bs.read(bytes);
        ByteBuf buffer = Unpooled.copiedBuffer(bytes);
        return buffer;

    }
}