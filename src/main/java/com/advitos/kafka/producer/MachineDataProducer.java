package com.advitos.kafka.producer;

import com.advitos.datahandling.BinaryLabviewData;
import com.advitos.datahandling.FileParser;
import com.advitos.datahandling.labviewclasses.ClosedLoop;
import com.advitos.datahandling.labviewclasses.DigitalInput;
import com.advitos.datahandling.labviewclasses.NumericInput;
import com.advitos.datahandling.labviewclasses.ProcessImage;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class MachineDataProducer {

    @Autowired
    private FileParser fileParser;
    @Autowired
    private KafkaTemplate<String, BinaryLabviewData> kafkaMachineDataTemplate;

    public void sendData() {

        List<ProcessImage> processImages = fileParser.scanFileSystem();
        LocalDateTime start = LocalDateTime.now();
        try {
            for (int i = 0; i < processImages.size(); i++) {

                ProcessImage processImage = processImages.get(i);
                ArrayList<NumericInput> numericInputs = (ArrayList<NumericInput>) processImage.getNumericInputs();
                for (NumericInput numericInput : numericInputs) {
                    numericInput.setDevice(processImage.getDevice());
                    numericInput.setPi_timestamp(processImage.getSqlTimestamp());
                    kafkaMachineDataTemplate.send("numeric_input", numericInput);
                    log.info(String.format("\nXXXXX -> Produced message -> %s", numericInput));
                }
                ArrayList<ClosedLoop> closedLoops = (ArrayList<ClosedLoop>) processImage.getClosedLoops();
                for (ClosedLoop closedLoop: closedLoops) {
                    closedLoop.setDevice(processImage.getDevice());
                    closedLoop.setPi_timestamp(processImage.getSqlTimestamp());
                    kafkaMachineDataTemplate.send("closed_loop", closedLoop);
                    log.info(String.format("\n##### -> Produced message -> %s", closedLoop));
                }
                ArrayList<DigitalInput> digitalInputs = (ArrayList<DigitalInput>) processImage.getDigitalInputs();
                for (DigitalInput digitalInput: digitalInputs) {
                    digitalInput.setDevice(processImage.getDevice());
                    digitalInput.setPi_timestamp(processImage.getSqlTimestamp());
                    kafkaMachineDataTemplate.send("digital_input", digitalInput);
                    log.info(String.format("\n+++++ -> Produced message -> %s", digitalInput));
                }
                Thread.sleep(250);

            }
        } catch (IllegalArgumentException | InterruptedException e) {
            log.error(e.getMessage());
        }
        LocalDateTime end = LocalDateTime.now();
        Duration d = Duration.between(start, end);
        log.info("##### --> DATA SENT DURATION: " + d.toSeconds() + " SECONDS");
    }
}
