package com.advitos.kafka.producer;

import com.advitos.datahandling.BinaryLabviewData;
import com.advitos.datahandling.hostcomm.HCProcessImage;
import com.advitos.datahandling.labviewclasses.ClosedLoop;
import com.advitos.datahandling.labviewclasses.DigitalInput;
import com.advitos.datahandling.labviewclasses.NumericInput;
import com.advitos.datahandling.labviewclasses.ProcessImage;
import io.netty.buffer.ByteBuf;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Log4j2
@Service
public class ProcessImageProducer {

    @Autowired
    private KafkaTemplate<String, BinaryLabviewData> kafkaMachineDataTemplate;

    public void sendData(ByteBuf byteBuffer) {

        try {

            HCProcessImage processImage = new HCProcessImage();
            // ProcessImage processImage = new ProcessImage();
            log.info("PRODUCER BYTE BUFFER: " + byteBuffer.capacity());
            processImage.parse(byteBuffer);
            log.info(processImage);

            /* ArrayList<NumericInput> numericInputs = (ArrayList<NumericInput>) processImage.getNumericInputs();
            for (NumericInput numericInput : numericInputs) {
                // numericInput.setDevice(processImage.getDevice());
                // numericInput.setPi_timestamp(processImage.getSqlTimestamp());
                // kafkaMachineDataTemplate.send("numeric_input", numericInput);
                // log.info(String.format("\nXXXXX -> Produced message -> %s", numericInput));
            }
            ArrayList<ClosedLoop> closedLoops = (ArrayList<ClosedLoop>) processImage.getClosedLoops();
            for (ClosedLoop closedLoop : closedLoops) {
                // closedLoop.setDevice(processImage.getDevice());
                // closedLoop.setPi_timestamp(processImage.getSqlTimestamp());
                // kafkaMachineDataTemplate.send("closed_loop", closedLoop);
                // log.info(String.format("\n##### -> Produced message -> %s", closedLoop));
            }
            ArrayList<DigitalInput> digitalInputs = (ArrayList<DigitalInput>) processImage.getDigitalInputs();
            for (DigitalInput digitalInput : digitalInputs) {
                // digitalInput.setDevice(processImage.getDevice());
                // digitalInput.setPi_timestamp(processImage.getSqlTimestamp());
                // kafkaMachineDataTemplate.send("digital_input", digitalInput);
                // log.info(String.format("\nYYYYY -> Produced message -> %s", digitalInput));
            }

             */


        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
        }

    }
}

